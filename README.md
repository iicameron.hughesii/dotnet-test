## Human Resources App
### Introduction
This repository provides a starting point which will allow you to create a HR System for a fictional company. It also contains a set of mini tasks designed to test your debugging skills. In order for you to complete this challenge, you MUST continue with at least one of the given Windows Forms Projects (your choice C# or VB) and add code to complete defined tasks.

### System Requirements
To get started, you'll need the following:

- [Visual Studio](https://visualstudio.microsoft.com/downloads/)
- git

### Project Requirements
The app has already been setup and should display people employed in the company, for this it uses SQLite database (`hr.db`) and  [SQLite-net library](https://github.com/praeclarum/sqlite-net) to connect and query it. You are free to replace the library or use any other free-ware nuget package that you wish. However the SQLite database must stay. Below is what we will be looking for in a completed solution:

- Evidence of logic
- Usability of UI
- Clean, reusable and testable code
- Ability to debug

Extra marks will be awarded for:
- Using built-in functionality such as
  - Async
  - Linq
  - Exception handling
  - .NET API
- Encapsulation
- Evidence of [SOLID](https://en.wikipedia.org/wiki/SOLID) design
- Using [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) principle
- Adding unit tests
- Improving pre-written code and UI
- Using SQL

#### Task 1
Implement below functionality:

- Add/Modify/Delete users
- Display user's Department on the MainForm
- Add/Modify/Delete departments
- Add ability to filter employee list to Heads of Department only

#### Task 2
There is a link (`Next Task`) to take you to Task 2 on the MainForm which must stay. You will attempt to complete each sub task, by amending current code, replacing it and filling in the blanks.

#### Task 3
Add evidence of testing for above tasks, if this takes in form of a document commit it into the repository as part of submission.

### Submission
To submit your solution, please fork this repository and provide us a link to your finished version.

### Copyright
All trademarks as the property of their respective owners.